module.exports = {
    mode: "development",
    devtool: "inline-source-map",
    entry:  "./src/index.js",
    output: {

        path: path.resolve(__dirname, 'dist'),
        filename: 'mylibrary.min.js',
        libraryTarget: 'umd',
        library: 'mylibrary'
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: [ ".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
            { test: /\.jsx?&/ }
        ]
    }
}